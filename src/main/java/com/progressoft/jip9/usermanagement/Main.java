package com.progressoft.jip9.usermanagement;

public class Main {
/**************************************************************************/
    public static void main(String[] args) {
        DataBase arrayListDB = new ArrayListDataBase();
        UsersManagement usersManagement = new UsersManagement(arrayListDB);
        ConsoleUserManagement consoleUser = new ConsoleUserManagement(usersManagement);
        consoleUser.promptUser();

    }
}
