package com.progressoft.jip9.usermanagement;

import java.util.ArrayList;
public interface DataBase {

    ArrayList<UserData> displayAllUsers();
    void addNewUser(UserData newUser);
    boolean enableUserName(String userName);
    boolean disableUserName(String userName);
    void resetUserPassword(String username, String newPassword);
    boolean validateUserAndEmail(String userName, String email);
    boolean IsExistUserNameOrEmail(String userName, String email, int i);
}
