package com.progressoft.jip9.usermanagement;

import com.sun.org.apache.bcel.internal.generic.ATHROW;
import org.apache.commons.lang3.ObjectUtils;

import java.util.ArrayList;
import java.util.Objects;


public class ArrayListDataBase implements DataBase {

    private ArrayList<UserData> usersList = new ArrayList<>();

    public boolean validateUserAndEmail(String userName, String email) {
        for (int i = 0; i < usersList.size(); i++) {
            if (IsExistUserNameOrEmail(userName, email, i))
                return false;
        }
        return true;
    }

    public boolean IsExistUserNameOrEmail(String userName, String email, int i) {
        if (userName.equals(usersList.get(i).getUsername()) || email.equals(usersList.get(i).getEmail())) {
            return true;
        }
        return false;
    }

    public void addNewUser(UserData newUser) throws IllegalArgumentException {
        if (!validateUserAndEmail(newUser.getName(), newUser.getEmail())) {
            throw new IllegalArgumentException("Username or email is already exist");
        }
        usersList.add(newUser);
    }


    public boolean enableUserName(String userName) throws IllegalArgumentException {
        for (int i = 0; i < usersList.size(); i++) {
            if (userName.equals(usersList.get(i).getUsername())) {
                return checkActiveStatus(i);
            }

        }
        throw new IllegalArgumentException("Invaild user-name");
    }

    private boolean checkActiveStatus(int i) {
        if (usersList.get(i).getActiveStatus()) {
            return false;
        }
        usersList.get(i).setActive(true);
        return true;

    }

    public boolean disableUserName(String userName) {
        for (int i = 0; i < usersList.size(); i++) {
            if (userName.equals(usersList.get(i).getUsername())) {
                return checkInActiveStatus(i);
            }

        }
        throw new IllegalArgumentException("Invaild user-name");
    }

    private boolean checkInActiveStatus(int i) {
        if (!usersList.get(i).getActiveStatus()) {
            return false;
        }
        usersList.get(i).setActive(false);
        return true;
    }
    public ArrayList<UserData> displayAllUsers() {
        if (usersList.size() == 0) {
            throw new IllegalArgumentException("List is Empty");
        }
        return usersList;
    }



    public void resetUserPassword(String username, String newPassword) {
        for (int i = 0; i < usersList.size(); i++) {
            if (username.equals(usersList.get(i).getUsername())) {
                usersList.get(i).setPassword(newPassword);
            }
        }
        throw new IllegalArgumentException("Invaild user-name");
    }
}
