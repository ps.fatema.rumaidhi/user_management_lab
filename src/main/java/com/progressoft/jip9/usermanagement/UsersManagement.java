package com.progressoft.jip9.usermanagement;

import java.util.ArrayList;
import java.util.Arrays;

public class UsersManagement {
    private DataBase dataBase;
    private RandomPassword randomPassword = new RandomPassword();

    public UsersManagement(DataBase dataBase) {
        this.dataBase = dataBase;
    }


    protected void exist() {
        System.exit(0);
    }

    protected int validateUserChoose(int userChoose) throws IllegalArgumentException {
        boolean trueChoose = userChooseWithinTheRange(userChoose);
        if (!trueChoose) {
            throw new IllegalArgumentException("Please enter a number from 1 - 6 only");
        }
        return userChoose;
    }


    private boolean userChooseWithinTheRange(int userChoose) {
        int[] chooses = {1, 2, 3, 4, 5, 6};
        int found = Arrays.binarySearch(chooses, userChoose);
        return found > -1;
    }

    protected String resetPassword(String userName) {
        String password = randomPassword.generateRandomPassword();
        dataBase.resetUserPassword(userName, password);
        return password;
    }

    protected String disableUser(String userName) {
        if (!dataBase.disableUserName(userName)) {
            // return "user already inactive";
            throw new IllegalArgumentException("User already inactive");
        }
        return "User " + userName + "  now is inactive";
    }

    protected String enableUser(String userName) {
        if (dataBase.enableUserName(userName)) {
            return "User " + userName + " Now is Active";
        }
        throw new IllegalArgumentException("user already active");
    }


    public String addNewUser(String userInformation) {
        String[] newUser = userInformation.split(",");
        throwIfInvalidInputFormat(newUser);
        String password = randomPassword.generateRandomPassword();
        UserData user = new UserData(newUser[0], newUser[1], newUser[2], password, false);
        dataBase.addNewUser(user);
        return password;
    }

    private void throwIfInvalidInputFormat(String[] newUser) {
        if (newUser.length > 3 || newUser.length < 3) {
            throw new ArrayIndexOutOfBoundsException(" Please  make sure you enter a user-name.Also a semicolon between each\ninput and another(name , user-name ,  e-mail ) and check you" +
                    "have enter three inputs\nno less no more which are : name , user-name ,  e-mail ");
        }
    }
/*
    protected void displayAllUsers() {
        dataBase.displayAllUsers();
    }

 */

    public ArrayList<UserData> displayAllUsers() {
        return dataBase.displayAllUsers();
    }
}
