package com.progressoft.jip9.usermanagement;


import org.apache.commons.lang3.RandomStringUtils;

public class RandomPassword {


    protected String generateRandomPassword() {
        String lettersFirstgroup = RandomStringUtils.randomAlphabetic(2);
        String lettersSecondgroup = RandomStringUtils.randomAlphabetic(1);
        String numbersFirstgroup = RandomStringUtils.randomNumeric(2);
        String numberslastgroup = RandomStringUtils.randomNumeric(3);
        return lettersFirstgroup + numbersFirstgroup + lettersSecondgroup + numberslastgroup;

    }
}
