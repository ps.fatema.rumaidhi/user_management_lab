package com.progressoft.jip9.usermanagement;

import java.util.ArrayList;
import java.util.Scanner;

public class ConsoleUserManagement{
    private UsersManagement usersManagement;


    //need to catch exceptions !!!! here/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11

    ConsoleUserManagement(UsersManagement usersManagement){
        this.usersManagement = usersManagement;
    }

    public int promptUser() throws IllegalArgumentException {

        Scanner input = new Scanner(System.in);
        int userChoose, validateChoose;
        do {
            displayMenu();
            userChoose = input.nextInt();
            validateChoose = usersManagement.validateUserChoose(userChoose);
            SwitchValidateUserChoose(validateChoose);
        } while (userChoose != 6);
        return validateChoose;
    }

    protected void SwitchValidateUserChoose(int validateChoose) {
        switch (validateChoose) {
            case 1:
                displayAll();
                break;
            case 2:
                displayPassword(usersManagement.addNewUser(promptUserForUserData()));
                break;
            case 3:
                try {
                    display(usersManagement.enableUser(enableUserPrompt()));
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
                break;
            case 4:

                try{
                display(usersManagement.disableUser(disableUserPrompt()));
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
                break;
            case 5:
                displayNewPassword(usersManagement.resetPassword(resetPasswordPrompt()));
                break;
            default:
                display("Exist Program");
               usersManagement.exist();

        }
    }

    private String disableUserPrompt() {
        System.out.println("Enter userName you want to disable");
        Scanner userInput = new Scanner(System.in);
        return  userInput.nextLine().trim();
    }

    private void  displayPassword(String password){
        System.out.println("Your Password is: " + password);
    }

    private void  displayNewPassword(String password){
        System.out.println("Your new Password is: " + password);
    }

    private String resetPasswordPrompt() {
        System.out.println("please enter your username to reset your password");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine().trim();
    }

    private String enableUserPrompt() {
        System.out.println("Enter user-name you want to enable");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine().trim();
    }

    private void displayMenu() {
        System.out.print("\n1- Display all users\n2- Add new user\n" +
                "3- Enable user\n4- Disable user\n" +
                "5- Reset Password\n" +
                "6- Exit\nPlease enter your option:\n ");
    }



    private String promptUserForUserData() {
        System.out.println("Enter User Information in the format (user, name, email):");
        System.out.println("example:HR, Human resource, hr@ps.com");
        Scanner userInput = new Scanner(System.in);
        return userInput.nextLine().trim();

    }
    public void displayAll() {


        printHeader();
        ArrayList<UserData> usersList= usersManagement.displayAllUsers();
        for (int i = 0; i < usersList.size(); i++) {

            String activeStatus;
            if (!usersList.get(i).getActiveStatus()) {
                activeStatus="No";
            } else {
                activeStatus="Yes";
            }
            System.out.printf("|%-15s | %-20s | %-17s | %-17s |\n", usersList.get(i).getUsername(),
                    usersList.get(i).getName(), usersList.get(i).getEmail(),activeStatus);
        }

        System.out.println("+-------------------------------------------------------------------------------+");
    }

    private void printHeader() {
        System.out.println("+-------------------------------------------------------------------------------+");
        System.out.printf("|%-15s | %-20s | %-17s | %-17s |\n","Username","Name","E-mail","Active(yes/no)");
        System.out.println("+-------------------------------------------------------------------------------+");
    }


    public void display(String displayString){
        System.out.println(displayString);
    }

    public void userChangeStatus(String status){
        System.out.println("User status change to "+ status);
    }



}


