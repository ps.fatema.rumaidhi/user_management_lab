package com.progressoft.jip9.usermanagement;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UsersManagementTest {
    private DataBase db_ArrayList = new ArrayListDataBase();
    private UsersManagement usersManagement = new UsersManagement(db_ArrayList);

    @Test
    public void givenInValidNumber_whenPromptUser_thenThrowsIllegalArgumentException() {
        int userChoose = 9;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class
                , () -> usersManagement.validateUserChoose(userChoose));
        Assertions.assertEquals("Please enter a number from 1 - 6 only", exception.getMessage());
    }

    @Test
    public void givenEmptyArrayList_whenDispllayAllUsers_thenThrowsIllegalArgumentException() {
        Exception ex = Assertions.assertThrows(IllegalArgumentException.class, () -> usersManagement.displayAllUsers());
        Assertions.assertEquals("List is Empty", ex.getMessage());
    }

    @Test
    public void givenAlreayExistUser_whenAddNewUser_thenThrowsIllegalArgumentException() {
        // TODO test if duplicate username and duplicate email
        usersManagement.addNewUser("HR, Human resource, hr@ps.com");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> usersManagement.addNewUser("HR, Human resource, hr@ps.com"));
        Assertions.assertEquals("Username or email is already exist", exception.getMessage());

        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> usersManagement.addNewUser("HR1, Human resource, hr@ps.com"));
        Assertions.assertEquals("Username or email is already exist", exception.getMessage());
    }

    @Test
    public void givenUserInputWithOutSemicolon_whenAddNewUser_thenThrowsArrayIndexOutOfBoundsException() {
        IndexOutOfBoundsException exception = Assertions.assertThrows(IndexOutOfBoundsException.class, () -> usersManagement.addNewUser("HR   Human resource  hr@ps.com"));
        Assertions.assertEquals(" Please  make sure you enter a user-name.Also a semicolon between each\ninput and another(name , user-name ,  e-mail ) and check you" +
                "have enter three inputs\nno less no more which are : name , user-name ,  e-mail ", exception.getMessage());

    }

    @Test
    public void givenMoreNumberOfUserInput_whenAddNewUser_thenThrowsArrayIndexOutOfBoundsException() {
        //more input
        IndexOutOfBoundsException exception = Assertions.assertThrows(IndexOutOfBoundsException.class, () -> usersManagement.addNewUser("Nora, HR, Nora , hr@ps.com"));
        Assertions.assertEquals((" Please  make sure you enter a user-name.Also a semicolon between each\ninput and another(name , user-name ,  e-mail ) and check you" +
                "have enter three inputs\nno less no more which are : name , user-name ,  e-mail "), exception.getMessage());
    }


    @Test
    public void givenLessNumberOfInputs_whenAddNewUser_thenThrowsArrayIndexOutOfBoundsException() {
        //less  input
        IndexOutOfBoundsException exception = Assertions.assertThrows(IndexOutOfBoundsException.class, () -> usersManagement.addNewUser("HR,  hr@ps.com"));
        Assertions.assertEquals(" Please  make sure you enter a user-name.Also a semicolon between each\ninput and another(name , user-name ,  e-mail ) and check you" +
                "have enter three inputs\nno less no more which are : name , user-name ,  e-mail ", exception.getMessage());

    }


    @Test
    public void givenVaildUserInput_whenAddNewUser_thenAddNewUserToArrayList() {

    }


    @Test
    public void givenEmptyUserNameOrMoreOrLessNumberOfUserInput_whenEnableUser_thenThrowsIllegalArgumentException() {
        //end-user did not specify any userName, he press enter only
        IndexOutOfBoundsException exception = Assertions.assertThrows(IndexOutOfBoundsException.class, () -> usersManagement.addNewUser("Nora, HR, Nora , hr@ps.com"));
        Assertions.assertEquals(" Please  make sure you enter a user-name.Also a semicolon between each\ninput and another(name , user-name ,  e-mail ) and check you" +
                "have enter three inputs\nno less no more which are : name , user-name ,  e-mail ", exception.getMessage());
    }


    @Test
    public void givenEnableUser_whenEnableUser_thenThrowsIllegalArgumentException() {
        usersManagement.addNewUser("Adnan,Adnan,Adnan231Naser@gmail.com");
       usersManagement.enableUser("Adnan");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> usersManagement.enableUser("Adnan"));
        Assertions.assertEquals("user already active", exception.getMessage());


    }
    
    @Test
    public void givenEmptyOrInvalidUserName_whenEnableUser_thenThrowsIllegalArgumentException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> usersManagement.enableUser(" "));
        Assertions.assertEquals("Invaild user-name", exception.getMessage());
        IllegalArgumentException ex = Assertions.assertThrows(IllegalArgumentException.class,
                () -> usersManagement.enableUser("o"));
        Assertions.assertEquals("Invaild user-name", ex.getMessage());
    }

    @Test
    public void givenMoreNumberOfUserInput_whenEnableUser_thenThrowsIllegalArgumentException() {


    }

    @Test
    public void givenValidUserName_whenEnableUser_thenEnabletheValidUserInTheArrayList() {
        usersManagement.addNewUser("Nora, Nora , hr@ps.com");
        String expected=  "User Nora Now is Active";
        String actual =usersManagement.enableUser("Nora");
        Assertions.assertEquals(expected,actual);

    }


    @Test
    public void givenDisableUser_whenDisableUser_thenThrowsIllegalArgumentException() {
        usersManagement.addNewUser("n, n , hr@ps.com");
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> usersManagement.disableUser("n"));
        Assertions.assertEquals("User already inactive", exception.getMessage());
    }

    @Test
    public void givenInvalidUserName_whenDisableUser_thenThrowsIllegalArgumentException() {
        //Invalid Or Unexist UserName in DB
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> usersManagement.disableUser(" "));
        Assertions.assertEquals("Invaild user-name", exception.getMessage());

        IllegalArgumentException ex = Assertions.assertThrows(IllegalArgumentException.class,
                () -> usersManagement.disableUser("o"));
        Assertions.assertEquals("Invaild user-name", ex.getMessage());
    }


    @Test
    public void givenMoreNumberOfUserInput_whenDisableUser_thenThrowsIllegalArgumentException() {
        //more user input

    }

    @Test
    public void givenValidUserName_whenDisableUser_thenDisableUserInTheArrayList() {

    }


    @Test
    public void givenInvalidUserName_whenResetPassword_thenThrowsIllegalArgumentException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> usersManagement.resetPassword("o"));
        Assertions.assertEquals("Invaild user-name", exception.getMessage());

    }

    @Test
    public void givenMoreNumberOfUserInput_whenResetPassword_thenThrowsIllegalArgumentException() {
        //more user input

    }

    @Test
    public void givenValidUserName_whenResetPassword_thenResetUserPassword() {


    }


}
